package com.ed.model;

public class ResponseModel {
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ResponseModel [status=" + status + "]";
	}

}
