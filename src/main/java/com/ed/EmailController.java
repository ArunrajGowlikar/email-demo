package com.ed;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ed.model.RequestModel;
import com.ed.model.ResponseModel;

@RestController
public class EmailController {
	
	@Autowired
	EmailService service;
	
	@PostMapping(path = {"/send"})
	public ResponseModel sendMail(@RequestBody RequestModel request) {
		Map<String, String> model = new HashMap<>();
		model.put("name", request.getName());
		ResponseModel result = null;
		try {
			result = service.sendMail(request, model);
		}catch(Exception e) {
			result = new ResponseModel();
			result.setStatus("Unable to send mail");
		}
		return result;
	}
}
