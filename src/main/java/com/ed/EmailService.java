package com.ed;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.ed.model.RequestModel;
import com.ed.model.ResponseModel;

import freemarker.template.Configuration;
import freemarker.template.Template;

@Service
public class EmailService {
	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	Configuration configuration;
	
	public ResponseModel sendMail(RequestModel request, Map<String,String> model) throws Exception {
		ResponseModel response = new ResponseModel();
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
		Template template = configuration.getTemplate("email-template.ftl");
		
		String text = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
		
		
		helper.setFrom(request.getFrom());
		helper.setTo(request.getTo());
		helper.setText(text,true);
		helper.setSubject(request.getSubject());
		mailSender.send(mimeMessage);
		
		response.setStatus("mail send succesfully");
		return response;
	}
}
